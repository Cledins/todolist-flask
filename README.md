# TODO app

## Install dependencies

```bash
python -m pip install -r requirements.txt
```

## Run tests

```bash
python -m pytest
```

## Run the application

```bash
export PYTHONPATH=$PYTHONPATH:$PWD/src/todolist # add project root package to python path
python -m flask --app src/todolist/app/todo run --host 0.0.0.0
```

Swagger documentation is available at http://localhost:5000/

test