from flask import Flask
from flask_cors import CORS
from flask_restx import Api
from store.todo import TodoStore
from typing import Tuple
import sys

# this is a pointer to the module object instance itself.
this = sys.modules[__name__]

this.app: Flask = None
this.api: Api = None
this.store: TodoStore = None

def get_app() -> Flask:
    if not this.app:
      this.app = Flask(__name__)
      CORS(this.app, resources={r"/api/*": {"origins": "*"}})
    return this.app

def get_api() -> Api:
    if not this.api:
        this.api = Api(get_app(), 
          version='1.0', 
          title='Todo API',
          description='Simple API for management of Todos',
         )
    return this.api

def get_store() -> TodoStore:
    if not this.store:
        this.store = TodoStore()
    return this.store
